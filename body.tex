\usetheme{default}
\usecolortheme{default}

\usepackage{graphicx,amsmath,fontspec,hyperref,xcolor}

\setsansfont[
  Path=fonts/,
  Ligatures=TeX,
  BoldFont = SourceSansPro-Bold,
  ItalicFont = SourceSansPro-It,
  BoldItalicFont = SourceSansPro-BoldIt
  ]{SourceSansPro-Regular}

\newcommand{\nodagger}{\phantom{\dagger}}
\newcommand{\noprime}{\phantom{\prime}}
\newcommand{\nostar}{\phantom{*}}
\newcommand{\imi}{\textrm{\textit{i}}}
\newcommand{\expo}[1]{\mathrm{e}^{#1}}
\newcommand{\vect}[1]{\boldsymbol{#1}}
\newcommand{\matr}[1]{\mathbf{#1}}
\newcommand{\doubleColumnWidth}{0.49\linewidth}
\newcommand{\tripleColumnWidth}{0.32\linewidth}

\newcommand{\normalFigureHeight}{7em}

\definecolor{myRed}{HTML}{FF0C00}
\definecolor{myGreen}{HTML}{019301}
\definecolor{lightBlue}{rgb}{0.12156862745098039, 0.46666666666666667, 0.70588235294117652}
\definecolor{myOrange}{rgb}{1.0, 0.49803921568627452, 0.054901960784313725}

\graphicspath{{images/}}

\title{Generalised classes of continuous symmetries in two-mode Dicke models}
\author{Ryan Moodie}
\date{March 2018}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Outline}
  \tableofcontents
\end{frame}

\section[Dicke model]{Dicke model}

\begin{frame}
  \frametitle{Dicke model}
  \begin{equation*}
    \hat{H} = 
    \omega \textcolor{blue}{\hat{a}^{\dagger} \hat{a}}
    + \sum_{i = 1}^{N} \Big [ \omega_{0} \textcolor{myRed}{\hat{s}^{z}_{i}}
    + g \big ( \textcolor{blue}{\hat{a}^{\dagger}} \textcolor{myRed}{\hat{s}^{-}_{i}}
        + \textcolor{blue}{\hat{a}^{\dagger}} \textcolor{myRed}{\hat{s}^{+}_{i}}
    + \text{H.c.} \big ) \Big ]
  \end{equation*}
  \begin{itemize}
    \item <1-> \textcolor{myRed}{Two-level atoms} interacting with \textcolor{blue}{single light mode}
    % \item <3-> $\mathbf{Z}_{2}$ symmetry: $(\textcolor{blue}{\hat{a}},\textcolor{myRed}{\hat{s}^{+}_i})\rightarrow-(\textcolor{blue}{\hat{a}},\textcolor{myRed}{\hat{s}^{+}_i})$
    \item <2-> Superradiant phase transition
    \item <3-> Open system experiments
  \end{itemize}
  \begin{columns}
    \column{\tripleColumnWidth}
    \centering
    \begin{figure}
      \includegraphics[width=\linewidth]{dicke}
    \end{figure}
    {\tiny \phantom{B}}
    \column{\tripleColumnWidth}
    \centering
    \onslide<2->{
      \begin{figure}
      \includegraphics[width=\linewidth]{superradiance}
      \end{figure}
      }
    \column{\tripleColumnWidth}
    \centering
    \onslide<3->{
      \begin{figure}
        \includegraphics[width=\linewidth]{chequerboard}
      \end{figure}
      \tiny {Keeling et al., Phys. Rev. Lett. \textbf{105}, 043001 (2010)}
      }
  \end{columns}
\end{frame}

\section[U(1) symmetry in generalised model]{U(1) symmetry in generalised model}

\begin{frame}
  \frametitle{U(1) symmetry in generalised model}
  \only<1>{
    \begin{equation*}
      \hspace{-3ex}\hat{H} =
      \textcolor{lightBlue}{\omega_{a}} \textcolor{lightBlue}{\hat{a}^{\dagger} \hat{a}}
      + \textcolor{myOrange}{\omega_{b}} \textcolor{myOrange}{\hat{b}^{\dagger} \hat{b}}
      + \sum_{i=1}^{N} \Big [
      \omega_{0} \textcolor{red}{\hat{s}^{z}_{i}} +
       \big ( 
      g_a \textcolor{lightBlue}{\hat{a}^{\dagger}} \textcolor{red}{\hat{s}^{-}_{i}} + 
      g_b \textcolor{myOrange}{\hat{b}^{\dagger}} \textcolor{red}{\hat{s}^{-}_{i}} +
      g^\prime_a \textcolor{lightBlue}{\hat{a}^{\dagger}} \textcolor{red}{\hat{s}^{+}_{i}} +
      g^\prime_b \textcolor{myOrange}{\hat{b}^{\dagger}} \textcolor{red}{\hat{s}^{+}_{i}} 
      + \text{H.c.} \big ) \Big ]
    \end{equation*}
    }
  \only<2|handout:0>{
    \begin{equation*}
      \hspace{-1.5ex}\hat{H} =
      \textcolor{lightBlue}{\omega_{a}} \textcolor{lightBlue}{\hat{a}^{\dagger}} \textcolor{lightBlue}{\hat{a}}
      + \textcolor{myOrange}{\omega_{b}} \textcolor{myOrange}{\hat{b}^{\dagger}} \textcolor{myOrange}{\hat{b}}
      + \sum_{i=1}^{N} \bigg \{
      \omega_{0} \textcolor{red}{\hat{s}^{z}_{i}} + 
      g \Big [
        \big ( \textcolor{lightBlue}{\hat{a}^{\dagger}} + \imi \textcolor{myGreen}{\gamma} \textcolor{myOrange}{\hat{b}^{\dagger}} \big ) \textcolor{red}{\hat{s}^{-}_{i}}
        + \big ( \textcolor{myOrange}{\hat{b}^{\dagger}} + \imi \textcolor{myGreen}{\gamma} \textcolor{lightBlue}{\hat{a}^{\dagger}} \big ) \textcolor{red}{\hat{s}^{+}_{i}}
        + \text{H.c.} \Big ] \bigg \}
    \end{equation*}
    }
  \centering
  \begin{figure}
    \includegraphics[width=0.55\linewidth]{system}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{U(1) symmetry in generalised model}
  \begin{equation*}
    \hspace{-1.5ex}\hat{H} =
    \textcolor{lightBlue}{\omega_{a}} \textcolor{lightBlue}{\hat{a}^{\dagger}} \textcolor{lightBlue}{\hat{a}}
    + \textcolor{myOrange}{\omega_{b}} \textcolor{myOrange}{\hat{b}^{\dagger}} \textcolor{myOrange}{\hat{b}}
    + \sum_{i=1}^{N} \bigg \{
    \omega_{0} \textcolor{red}{\hat{s}^{z}_{i}} + 
    g \Big [
      \big ( \textcolor{lightBlue}{\hat{a}^{\dagger}} + \imi \textcolor{myGreen}{\gamma} \textcolor{myOrange}{\hat{b}^{\dagger}} \big ) \textcolor{red}{\hat{s}^{-}_{i}}
      + \big ( \textcolor{myOrange}{\hat{b}^{\dagger}} + \imi \textcolor{myGreen}{\gamma} \textcolor{lightBlue}{\hat{a}^{\dagger}} \big ) \textcolor{red}{\hat{s}^{+}_{i}}
      + \text{H.c.} \Big ] \bigg \}
  \end{equation*}
  \begin{itemize}[<+->]
    \item $\textcolor{myGreen}{\gamma}=0$. Shift phases: $\big(\textcolor{lightBlue}{\hat{a}}, \textcolor{myOrange}{\hat{b}^{\dagger}}, \textcolor{red}{s_i^-}\big)\rightarrow \expo{\imi\varphi}\big(\textcolor{lightBlue}{\hat{a}}, \textcolor{myOrange}{\hat{b}^{\dagger}}, \textcolor{red}{s_i^-}\big)$
    \item $\textcolor{myGreen}{\gamma}=1$. For $\textcolor{lightBlue}{\omega_{a}}=\textcolor{myOrange}{\omega_{b}}$, mix modes:
    \begin{equation*}
      \begin{pmatrix}
        \textcolor{lightBlue}{\hat{a}} \\
        \textcolor{myOrange}{\hat{b}}
      \end{pmatrix}
      \rightarrow
      \begin{pmatrix}
        \cos(\varphi)  & \sin(\varphi) \\
        -\sin(\varphi) & \cos(\varphi)
      \end{pmatrix}
      \begin{pmatrix}
        \textcolor{lightBlue}{\hat{a}} \\
        \textcolor{myOrange}{\hat{b}}
      \end{pmatrix}
    \end{equation*}
    \item $\textcolor{myGreen}{\gamma}=\tan(\textcolor{myGreen}{\chi})$. Generator is:
    \begin{equation*}
      \hat{G} =
      \cos(2 \textcolor{myGreen}{\chi})
      \big ( \textcolor{lightBlue}{\hat{a}^{\dagger} \hat{a}} - \textcolor{myOrange}{\hat{b}^{\dagger} \hat{b}} \big )
      - \imi \sin(2 \textcolor{myGreen}{\chi})
      \big ( \textcolor{lightBlue}{\hat{a}^{\dagger}} \textcolor{myOrange}{\hat{b}} - \textcolor{myOrange}{\hat{b}^{\dagger}} \textcolor{lightBlue}{\hat{a}} \big )
      + \hat{S}^{z}
    \end{equation*}
    \item Open system: $\hat{\rho} \rightarrow \hat U_\varphi^{\dagger} \hat{\rho} \hat U_\varphi$
    \begin{align*}
      \dot{\hat{\rho}} &= - \imi [\hat{H}, \hat{\rho}] + \frac{\kappa}{2} \left( \mathcal{L}[\textcolor{lightBlue}{\hat{a}}] + 
      \mathcal{L}[\textcolor{myOrange}{\hat{b}}] \right), &
      \mathcal{L}[\hat{x}] &= 2 \hat{x} \hat{\rho} \hat{x}^{\dagger} - \hat{x}^{\dagger} \hat{x} \hat{\rho} - \hat{\rho} \hat{x}^{\dagger} \hat{x}
    \end{align*}
  \end{itemize}
\end{frame}

\section[Equations of motion]{Equations of motion}

\begin{frame}
  \frametitle{Equations of motion}
  \begin{itemize}
    \item <1-> Mean-field theory ($N \sim 10^5$):
    \vspace{-0.5em}
    \begin{align*}
      \textcolor{lightBlue}{\alpha} =& \langle \textcolor{lightBlue}{\hat{a}} \rangle & 
      \textcolor{myOrange}{\beta} =& \langle \textcolor{myOrange}{\hat{b}} \rangle & 
      \vect{S} =& \left \langle \sum_{i=1}^{N} \hat{\vect{s}}_{i} \right \rangle &
      \left \langle \hat{x} \, \hat{y} \right \rangle &= \left \langle \hat{x} \right \rangle \left \langle \hat{y} \right \rangle
    \end{align*}
    \item <2-> $\langle \hat{x} \rangle = Tr(\hat{\rho} \, \hat{x}) 
    \quad \Rightarrow \quad 
    \frac{d}{d t} \langle \hat{x} \rangle = Tr(\dot{\hat{\rho}} \, \hat{x})$
    \vspace{0.5em}
    \item <3-> Steady state solution:
    \begin{multline*}
      \hspace{-4ex}S^{z} = - \frac{\omega_{0}}{2 g^{2}} \Big [
      \left ( 1 + \textcolor{myGreen}{\gamma}^{2} \right ) \left (
      \textcolor{myOrange}{\omega_{b} \zeta_b} + \textcolor{lightBlue}{\omega_{a} \zeta_a} \right )
      + \imi \frac{\kappa}{2} \left ( 1 - \textcolor{myGreen}{\gamma}^{2} \right ) \left (
      \textcolor{myOrange}{\zeta_b} - \textcolor{lightBlue}{\zeta_a} \right ) \\
      + \imi 2 \textcolor{myGreen}{\gamma} \expo{- \imi 2 \theta} \left (
      \textcolor{myOrange}{\omega_{b} \zeta_b} - \textcolor{lightBlue}{\omega_{a} \zeta_a}
      \right ) \Big ]^{-1} \quad \text{where} \enspace
      \zeta_{\textcolor{lightBlue}{a},\textcolor{myOrange}{b}} = \frac{1}{{\omega_{\textcolor{lightBlue}{a},\textcolor{myOrange}{b}}}^{2}+\frac{\kappa^{2}}{4}}
    \end{multline*}
    \vspace{-1em}
    \item <4-> Linear stability: 
    \begin{itemize}
      \item $\delta \alpha = a \expo{-\imi \lambda t} + b^{*} \expo{\imi \lambda^{*} t}$
      \item $\lambda \vect{v} = \matr{M} \, \vect{v}$
      \item stable iff $\: \operatorname{Im}(\lambda) < 0 \enspace \forall \lambda$
    \end{itemize}
  \end{itemize}
\end{frame}

\section[Phase diagrams]{Phase diagrams}

\begin{frame}
  \frametitle{Phase diagrams}
  \begin{columns}
    \column{\doubleColumnWidth}
    \centering
    \onslide<1->{
      \begin{figure}
        \includegraphics[width=\linewidth]{phases}
      \end{figure}
      }
    \column{\doubleColumnWidth}
    \centering
    \onslide<2->{
      \begin{figure}
        \includegraphics[width=0.55\linewidth]{zero-gamma}
      \end{figure}
      }
    \onslide<3->{
      \begin{figure}
        \includegraphics[width=0.9\linewidth]{limit-cycle}
      \end{figure}
      }
  \end{columns}
  \centering\tiny\vspace{1em}
  \href{https://link.aps.org/doi/10.1103/PhysRevA.97.033802}{Moodie et al., Phys. Rev. A. \textbf{97}, 033802 (2018)}
\end{frame}

\end{document}
