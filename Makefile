DOCNAME=presentation
HANDOUT=$(DOCNAME)_handout
TEX=xelatex

all: presentation handout

.PHONY: clean view view-handout

presentation:
	$(TEX) $(DOCNAME).tex

handout:
	$(TEX) $(HANDOUT).tex

view: 
	evince $(DOCNAME).pdf &

view-handout: 
	evince $(HANDOUT).pdf &

clean:
	rm *.blg *.bbl *.aux *.log *.out *.toc

